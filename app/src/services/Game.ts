import { findIndex } from 'rxjs';
import { MovePos } from './MovePos';
import { Reader } from './game/Reader';
import { Stack } from './game/Stack';
import { Maps } from './game/Maps';
import { direction } from './game/data/constantes';
import { ElementRef } from '@angular/core';

export class Game
{
 
  
  
  public play: boolean = true;
  size = 12;
  grid: any[] = [];
  item = {
    star: false,
    rocket: false,
    id: 0,
    disable: false,
    color: 0
  };
  rocket = {
    rotation: direction.RIGHT,
    pos: 15
  }
  startPosition: number = 15;

  
  speed: number = 2000;

  inst: any[] = [];
  instSelected = {
    f: 0,
    item: 0
  }
  instDefault = {
    move: null,
    func: null,
    selected: false,
    color: null,
    paint: null
  }
  movePos: any;
  f0Length = 4;
  f1Length = 4;
  f2Length = 4;
  currentInst = {
    f: 0,
    item: 0
  };
  allowPaintRed = true;
  allowPaintGreen = true;
  allowPaintBlue = true;
  allowTurnLeft = true;
  allowMoveForward = true;
  allowTurnRight = true;
  allowFunction0 = true;
  allowFunction1 = true;
  allowFunction2 = true;
  allowConditionRed = true;
  allowConditionGreen = true;
  allowConditionBlue = true;
  allowBrushRed = true;
  allowBrushGreen = true;
  allowBrushBlue = true;
  level: number = 1;
  public audioPlayer!: ElementRef<HTMLAudioElement>;

  reader!: Reader;
  stack!: Stack;
  maps!: Maps;
  constructor()
  {
    
    this.stack = new Stack(this);
    this.maps = new Maps(this);
    const game = this;
    this.movePos = new MovePos(game, this.grid, this.size, this.rocket);
  }

  setAudioPlayer(audioPlayer: ElementRef<HTMLAudioElement>) {
    this.audioPlayer = audioPlayer;
  }
  onPlayAudio() {

    const audioPlayer = this.audioPlayer.nativeElement;
    console.log('this.play', this.play);
    
    if (this.play) {
      audioPlayer.pause();
      this.play = false;
    } else {
      audioPlayer.play();
      this.play = true;
    }
  }
  

  createMap(start: number, stars: any[], outOfPath: any[], colors: any[]) {
    let { size, item } = this;
    // size = 8;

    this.grid = [];
    this.rocket.pos = start;

    for (let index = 0; index < size * size; index++) {

      const elm = { ...item };

      if (index+1 === start) {
        elm.rocket = true;
      }
      else if (outOfPath.includes(index+1)) {
        elm.disable = true;
      }
      else if (stars.includes(index+1)) {
        elm.star = true;
      }
      
      if (colors.length) {

        const color = colors.find((color) => color[0] === index+1);
        
        if (color)
        {
          if (color[1] === 1) elm.color = 1; // red
          else if (color[1] === 2) elm.color = 2; // green
          else if (color[1] === 3) elm.color = 3; // blue
        }
      }

      elm.id = index;
      this.grid.push(elm);
    }
  }

  clearMap()
  {
    this.grid = [];
    // const { size, item } = this;
    // const elm = { ...item };
    // for (let index = 0; index < size * size; index++) {
    //   this.grid.push(elm);
    // }
  }



  addTurnLeft() {
    const f = this.instSelected.f;
    const item = this.instSelected.item;
    this.inst[f][item].move = 'Left';
    this.inst[f][item].func = null;
  }
  addUp() {
    const f = this.instSelected.f;
    const item = this.instSelected.item;
    this.inst[f][item].move = 'Up';
    this.inst[f][item].func = null;
  }
  addTurnRight() {
    const f = this.instSelected.f;
    const item = this.instSelected.item;
    this.inst[f][item].move = 'Right';
    this.inst[f][item].func = null;
  }

  addFunc(idFunc: number)
  {
    
    if (idFunc === 0 && !this.allowFunction0) return
    if (idFunc === 1 && !this.allowFunction1) return;
    if (idFunc === 2 && !this.allowFunction2) return;

    const f = this.instSelected.f;
    const item = this.instSelected.item;
    this.inst[f][item].move = null;
    this.inst[f][item].func = idFunc;
  }

  addColor(colorId: number)
  {
    
    if (colorId === 1 && !this.allowConditionRed) return
    if (colorId === 2 && !this.allowConditionGreen) return;
    if (colorId === 3 && !this.allowConditionBlue) return;

    const f = this.instSelected.f;
    const item = this.instSelected.item;

    this.inst[f][item].color = this.inst[f][item].color !== colorId ? colorId : null;
  }
  addPaint(colorId: number)
  {

    if (colorId === 1 && !this.allowBrushRed) return
    if (colorId === 2 && !this.allowBrushGreen) return;
    if (colorId === 3 && !this.allowBrushBlue) return;

    const f = this.instSelected.f;
    const item = this.instSelected.item;

    this.inst[f][item].paint = this.inst[f][item].paint !== colorId ? colorId : null;
  }

  onChangeLevel(event: Event) {
    const select = event.target as HTMLInputElement;
    const value = select.value;
    console.log('value', value);
    this.level = +value;
    this.maps.create();
  }
}