import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PixelsToMapComponent } from './pixels-to-map/pixels-to-map.component';
import { GameComponent } from './game/game.component';
import { GameBuilderComponent } from './game-builder/game-builder.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: 'pixels-to-map', component: PixelsToMapComponent },
  { path: 'game-builder', component: GameBuilderComponent },
  { path: 'game', component: GameComponent },
  { path: 'home', component: HomeComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' } // Redirige vers 'pixels-to-map' par défaut
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
