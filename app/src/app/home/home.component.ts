import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  // https://pixabay.com/sound-effects/
  @ViewChild('audioPlayer', { static: true }) audioPlayer!: ElementRef<HTMLAudioElement>;
  public play: boolean = true;

  ngOnInit() {
    const audioPlayer = this.audioPlayer.nativeElement;
    audioPlayer.play();
    this.play = true;
  }

  stopAudio() {
    const audioPlayer = this.audioPlayer.nativeElement;
    audioPlayer.pause();
    this.play = false;
  }

  onPlayAudio() {

    const audioPlayer = this.audioPlayer.nativeElement;
    console.log('this.play', this.play);

    if (this.play) {
      audioPlayer.pause();
      this.play = false;
    } else {
      audioPlayer.play();
      this.play = true;
    }
  }
}
