
import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
const COLORS = {BLACK:0, RED:1, GREEN:2, BLUE:3};

@Component({
  selector: 'app-pixels-to-map',
  templateUrl: './pixels-to-map.component.html',
  styleUrls: ['./pixels-to-map.component.scss']
})
export class PixelsToMapComponent implements OnInit {
  @ViewChild('canvas', { static: true }) canvas!: ElementRef<HTMLCanvasElement>;
  @ViewChild('canvas2', { static: true }) canvas2!: ElementRef<HTMLCanvasElement>;
  pixelData: { r: number, g: number, b: number, a: number } | null = null;
  public cellsOutOfPath: string = '';
  public cellsColor: string = '';
  private ctx!: CanvasRenderingContext2D | null;
  private ctx2!: CanvasRenderingContext2D | null;
  isVisibleObjects: boolean = false;

  ngOnInit(): void {
    this.initCanvas1();
    this.initCanvas2();
  }

  onToogleVisibleObjects()
  {
    console.log('onToogleVisibleObjects !');
    this.isVisibleObjects = !this.isVisibleObjects;
  } 

  initCanvas1() {
    const canvas = this.canvas.nativeElement;
    canvas.width = 16;
    canvas.height = 16;

    canvas.addEventListener('click', this.handleCanvasClick.bind(this));

    this.ctx = canvas.getContext('2d');
    if (!this.ctx) {
      return;
    }

    this.ctx.fillStyle = 'black';
    this.ctx.fillRect(0, 0, 16, 16);

    this.ctx.fillStyle = 'red';
    this.ctx.fillRect(1, 1, 1, 1);
  }
  initCanvas2() {
    const canvas = this.canvas2.nativeElement;
    canvas.width = 16;
    canvas.height = 16;

    canvas.addEventListener('click', this.handleCanvasClick2.bind(this));

    this.ctx2 = canvas.getContext('2d');
    if (!this.ctx2) {
      return;
    }
    

    this.ctx2.fillStyle = 'yellow';
    this.ctx2.fillRect(0, 0, 1, 1);
  }

  handleCanvasClick(event: MouseEvent): void {
    const rect = this.canvas.nativeElement.getBoundingClientRect();
    const x = Math.floor((event.clientX - rect.left) / (rect.width / 16));
    const y = Math.floor((event.clientY - rect.top) / (rect.height / 16));
    // console.log('x',x, 'y',y);
    this.changeColor(x,y);
  }

  changeColor(x: number, y: number): void {
    if (!this.ctx) {
      return;
    }
    const imageData = this.ctx.getImageData(x, y, 1, 1);
    const data = imageData.data;
    console.log('data[0]', data[0], 'data[1]', data[1], 'data[2]', data[2],)
    const redColor = data[0] >= 255;
    const greenColor = data[1] == 128;
    const blueColor = data[2] >= 255;

    
    let colorIdx = 0;

    if (redColor && !greenColor && !blueColor) {
      colorIdx = 1;
    } else 
    if (!redColor && greenColor && !blueColor) {
      colorIdx = 2;
    } else 
    if (!redColor && !greenColor && blueColor) {
      colorIdx = 3;
    }
    
    console.log('colorIdx', colorIdx);
    if (colorIdx === COLORS.BLACK) {
      this.ctx.fillStyle = 'red';
    } else 
    if (colorIdx === COLORS.RED) {
      this.ctx.fillStyle = 'green';
    } else 
    if (colorIdx === COLORS.GREEN) {
      this.ctx.fillStyle = 'blue';
    } else 
    if (colorIdx === COLORS.BLUE) {
      this.ctx.fillStyle = 'black';
    }


    this.ctx.fillRect(x, y, 1, 1);
  }
  handleCanvasClick2(event: MouseEvent): void {
    const rect = this.canvas.nativeElement.getBoundingClientRect();
    const x = Math.floor((event.clientX - rect.left) / (rect.width / 16));
    const y = Math.floor((event.clientY - rect.top) / (rect.height / 16));
    // console.log('x',x, 'y',y);
    this.changeColor2(x,y);
  }

  changeColor2(x: number, y: number): void {
    if (!this.ctx) {
      return;
    }
    const imageData = this.ctx.getImageData(x, y, 1, 1);
    const data = imageData.data;
    console.log('data[0]', data[0], 'data[1]', data[1], 'data[2]', data[2],)
    const redColor = data[0] >= 255;
    const greenColor = data[1] >= 255;
    const blueColor = data[2] >= 255;

    
    let colorIdx = 0;

    if (redColor && greenColor && !blueColor) {
      colorIdx = 1;
    } else 
    if (redColor && !greenColor && blueColor) {
      colorIdx = 2;
    }
    
    console.log('colorIdx', colorIdx);
    if (colorIdx === 0) {
      this.ctx.fillStyle = 'yellow';
    } else 
    if (colorIdx === 1) {
      this.ctx.fillStyle = 'magenta';
    } else 
    if (colorIdx === 2) {
      this.ctx.fillStyle = 'black';
    } 


    this.ctx.fillRect(x, y, 1, 1);
  }

  onFileSelected(event: Event): void {
    const input = event.target as HTMLInputElement;
    if (!input.files || input.files.length === 0) {
      return;
    }

    const file = input.files[0];
    const img = new Image();
    img.src = URL.createObjectURL(file);

    img.onload = () => {
      const canvas = this.canvas.nativeElement;
      const ctx = canvas.getContext('2d');
      if (!ctx) {
        return;
      }

      canvas.width = img.width;
      canvas.height = img.height;
      ctx.drawImage(img, 0, 0);

      const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
      const data = imageData.data;

      // Lire les valeurs RGBA du premier pixel
      const r = data[0];
      const g = data[1];
      const b = data[2];
      const a = data[3];

      this.pixelData = { r, g, b, a };
      
      let i = 1;
      this.cellsOutOfPath = '[\n';
      for (let y = 0; y < canvas.height; y++) {
        for (let x = 0; x < canvas.width; x++) {
          const index = (y * canvas.width + x) * 4;
          const r = data[index];
          const g = data[index + 1];
          const b = data[index + 2];
          const a = data[index + 3];
          
          console.log(`Pixel (${x}, ${y}) - Rouge: ${r}, Vert: ${g}, Bleu: ${b}, Alpha: ${a}`);

          if (r === 0 && g === 0 && b === 0) {
            this.cellsOutOfPath += `${i},`;
            if (i % 16 === 0) this.cellsOutOfPath += "\n";
          }


          i++;
        }
      }
      this.cellsOutOfPath += ']';


      i = 1;
      this.cellsColor = '[\n';
      for (let y = 0; y < canvas.height; y++) {
        for (let x = 0; x < canvas.width; x++) {
          const index = (y * canvas.width + x) * 4;
          const r = data[index];
          const g = data[index + 1];
          const b = data[index + 2];
          const a = data[index + 3];
          
          console.log(`Pixel (${x}, ${y}) - Rouge: ${r}, Vert: ${g}, Bleu: ${b}, Alpha: ${a}`);

         
          
          if (r === 255) {
            this.cellsColor += `[${i}, 1],`;
            if (i % 16 === 0) this.cellsColor += "\n";
          }
          else if (g === 255) {
            this.cellsColor += `[${i}, 2],`;
            if (i % 16 === 0) this.cellsColor += "\n";
          }
          else if (b === 255) {
            this.cellsColor += `[${i}, 3],`;
            if (i % 16 === 0) this.cellsColor += "\n";
          }
          if (i % 16 === 0) this.cellsColor += "\n";

          i++;
        }
      }
      this.cellsColor += ']';




    };
  }
}
