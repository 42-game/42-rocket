import { Game } from "../Game";
import { direction } from "./data/constantes";
import {
    MAP_LVL1,
    MAP_LVL10,
    MAP_LVL11,
    MAP_LVL2,
    MAP_LVL4,
    MAP_LVL5,
    MAP_LVL6,
    MAP_LVL7,
    MAP_TEST,
    MAP_TEST14526,
    MAP_TEST3
} from "./data/maps/dataMaps";
import { MAP_TUTO1, MAP_TUTO2, MAP_TUTO3, MAP_TUTO4 } from "./data/maps/dataMapsTuto";

export class Maps {

    private game: Game;
    private emptyFunctions = true;

    constructor(game: Game) {
        this.game = game;
    }

    create() {
        const myNamesFunction = [
            'createMapTuto1',
            'createMapTuto2',
            'createMapTuto3',
            'createMapTuto4',
            'createMapLevel1',
            'createMapLevel2',
            'createMapLevel3',
            'createMapLevel4',
            'createMapLevel5',
            'createMapLevel6',
            'createMapLevel7',
            'createMapLevel10',
            'createMapLevel11',
            'createMapTest14526',
            'createMap4758'
        ];
        const str = myNamesFunction[this.game.level];
        (this as any)[str]();
    }
    reset() {
        this.emptyFunctions = false;
        this.create();
    }

    createMapTest() {
        this.game.size = 12;
        const grid: HTMLElement | null = document.querySelector('.grid-container');
        if (grid) {
            grid.style.gridTemplateColumns = 'repeat(auto-fill, minmax(8%, 1fr))';
            grid.style.gridTemplateRows = 'repeat(auto-fill, minmax(8%, 1fr))';
        }


        this.game.stack.initEmptyFunctions([4, 2, 3]);
        this.game.rocket.rotation = direction.RIGHT;
        this.game.createMap(...MAP_TEST);
    }

    
    createMapTuto1() {
        this.configSize16();

        this.game.allowTurnLeft = false;
        this.game.allowMoveForward = true;
        this.game.allowTurnRight = false;
        this.game.allowFunction0 = false;
        this.game.allowFunction1 = false;
        this.game.allowFunction2 = false;
        this.game.allowConditionRed = false;
        this.game.allowConditionGreen = false;
        this.game.allowConditionBlue = false;
        this.game.allowBrushRed = false;
        this.game.allowBrushGreen = false;
        this.game.allowBrushBlue = false;

        if (this.emptyFunctions)
            this.game.stack.initEmptyFunctions([3]);

        this.game.rocket.rotation = direction.RIGHT;
        this.game.createMap(...MAP_TUTO1);
    }

    createMapTuto2() {
        this.configSize16();

        this.game.allowTurnLeft = true;
        this.game.allowMoveForward = true;
        this.game.allowTurnRight = true;
        this.game.allowFunction0 = false;
        this.game.allowFunction1 = false;
        this.game.allowFunction2 = false;
        this.game.allowConditionRed = false;
        this.game.allowConditionGreen = false;
        this.game.allowConditionBlue = false;
        this.game.allowBrushRed = false;
        this.game.allowBrushGreen = false;
        this.game.allowBrushBlue = false;


        this.game.stack.initEmptyFunctions([4]);
        this.game.rocket.rotation = direction.RIGHT;
        this.game.createMap(...MAP_TUTO2);
    }

    createMapTuto3() {
        this.configSize16();

        this.game.allowTurnLeft = true;
        this.game.allowMoveForward = true;
        this.game.allowTurnRight = true;
        this.game.allowFunction0 = false;
        this.game.allowFunction1 = true;
        this.game.allowFunction2 = false;
        this.game.allowConditionRed = false;
        this.game.allowConditionGreen = false;
        this.game.allowConditionBlue = false;
        this.game.allowBrushRed = false;
        this.game.allowBrushGreen = false;
        this.game.allowBrushBlue = false;


        this.game.stack.initEmptyFunctions([3, 3]);
        this.game.rocket.rotation = direction.RIGHT;
        this.game.createMap(...MAP_TUTO3);
    }

    createMapTuto4() {
        this.configSize16();

        this.game.allowTurnLeft = true;
        this.game.allowMoveForward = true;
        this.game.allowTurnRight = true;
        this.game.allowFunction0 = true;
        this.game.allowFunction1 = false;
        this.game.allowFunction2 = false;
        this.game.allowConditionRed = true;
        this.game.allowConditionGreen = true;
        this.game.allowConditionBlue = true;
        this.game.allowBrushRed = false;
        this.game.allowBrushGreen = false;
        this.game.allowBrushBlue = false;


        this.game.stack.initEmptyFunctions([3]);
        this.game.rocket.rotation = direction.DOWN;
        this.game.createMap(...MAP_TUTO4);
    }

    createMapLevel1() {
        this.configSize16();

        this.game.allowTurnLeft = true;
        this.game.allowMoveForward = true;
        this.game.allowTurnRight = true;
        this.game.allowFunction0 = true;
        this.game.allowFunction1 = false;
        this.game.allowFunction2 = false;
        this.game.allowConditionRed = false;
        this.game.allowConditionGreen = false;
        this.game.allowConditionBlue = false;
        this.game.allowBrushRed = true;
        this.game.allowBrushGreen = true;
        this.game.allowBrushBlue = true;

        if (this.emptyFunctions)
            this.game.stack.initEmptyFunctions([2]);

        this.game.rocket.rotation = direction.DOWN;
        this.game.createMap(...MAP_LVL1);
    }

    createMapLevel2() {
        this.configSize16();

        this.game.allowTurnLeft = true;
        this.game.allowMoveForward = true;
        this.game.allowTurnRight = true;
        this.game.allowFunction0 = true;
        this.game.allowFunction1 = false;
        this.game.allowFunction2 = false;
        this.game.allowConditionRed = false;
        this.game.allowConditionGreen = false;
        this.game.allowConditionBlue = false;
        this.game.allowBrushRed = true;
        this.game.allowBrushGreen = true;
        this.game.allowBrushBlue = true;

        if (this.emptyFunctions)
            this.game.stack.initEmptyFunctions([5]);

        this.game.rocket.rotation = direction.DOWN;
        this.game.createMap(...MAP_LVL2);
    }

    createMapLevel3() {
        this.configSize16();

        this.game.allowTurnLeft = true;
        this.game.allowMoveForward = true;
        this.game.allowTurnRight = true;
        this.game.allowFunction0 = true;
        this.game.allowFunction1 = false;
        this.game.allowFunction2 = false;
        this.game.allowConditionRed = true;
        this.game.allowConditionGreen = true;
        this.game.allowConditionBlue = true;
        this.game.allowBrushRed = false;
        this.game.allowBrushGreen = false;
        this.game.allowBrushBlue = false;

        this.game.stack.initEmptyFunctions([5]);
        this.game.rocket.rotation = direction.DOWN;
        this.game.createMap(...MAP_TEST3);
    }

    createMapLevel4() {
        this.configSize16();

        this.game.allowTurnLeft = true;
        this.game.allowMoveForward = true;
        this.game.allowTurnRight = true;
        this.game.allowFunction0 = true;
        this.game.allowFunction1 = false;
        this.game.allowFunction2 = false;
        this.game.allowConditionRed = true;
        this.game.allowConditionGreen = true;
        this.game.allowConditionBlue = true;
        this.game.allowBrushRed = false;
        this.game.allowBrushGreen = true;
        this.game.allowBrushBlue = false;
        
        if (this.emptyFunctions)
            this.game.stack.initEmptyFunctions([6]);

        this.game.rocket.rotation = direction.UP;
        this.game.createMap(...MAP_LVL4);
    }

    createMapLevel5() {
        this.configSize16();

        this.game.allowTurnLeft = true;
        this.game.allowMoveForward = true;
        this.game.allowTurnRight = true;
        this.game.allowFunction0 = true;
        this.game.allowFunction1 = true;
        this.game.allowFunction2 = false;
        this.game.allowConditionRed = true;
        this.game.allowConditionGreen = true;
        this.game.allowConditionBlue = true;
        this.game.allowBrushRed = false;
        this.game.allowBrushGreen = true;
        this.game.allowBrushBlue = false;

        if (this.emptyFunctions)
            this.game.stack.initEmptyFunctions([4, 3]);

        this.game.rocket.rotation = direction.UP;
        this.game.createMap(...MAP_LVL5);
    }
    createMapLevel6() {
        this.configSize16();

        this.game.allowTurnLeft = true;
        this.game.allowMoveForward = true;
        this.game.allowTurnRight = true;
        this.game.allowFunction0 = true;
        this.game.allowFunction1 = true;
        this.game.allowFunction2 = false;
        this.game.allowConditionRed = true;
        this.game.allowConditionGreen = true;
        this.game.allowConditionBlue = true;
        this.game.allowBrushRed = false;
        this.game.allowBrushGreen = false;
        this.game.allowBrushBlue = false;

        if (this.emptyFunctions)
            this.game.stack.initEmptyFunctions([3, 3]);

        this.game.rocket.rotation = direction.RIGHT;
        this.game.createMap(...MAP_LVL6);
    }
    createMapLevel7() {
        this.configSize16();

        this.game.allowTurnLeft = true;
        this.game.allowMoveForward = true;
        this.game.allowTurnRight = true;
        this.game.allowFunction0 = true;
        this.game.allowFunction1 = false;
        this.game.allowFunction2 = false;
        this.game.allowConditionRed = true;
        this.game.allowConditionGreen = true;
        this.game.allowConditionBlue = true;
        this.game.allowBrushRed = false;
        this.game.allowBrushGreen = true;
        this.game.allowBrushBlue = false;

        if (this.emptyFunctions)
            this.game.stack.initEmptyFunctions([4]);

        this.game.rocket.rotation = direction.RIGHT;
        this.game.createMap(...MAP_LVL7);
    }
    createMapLevel10() {
        this.configSize16();

        this.game.allowTurnLeft = true;
        this.game.allowMoveForward = true;
        this.game.allowTurnRight = true;
        this.game.allowFunction0 = true;
        this.game.allowFunction1 = false;
        this.game.allowFunction2 = false;
        this.game.allowConditionRed = true;
        this.game.allowConditionGreen = true;
        this.game.allowConditionBlue = true;
        this.game.allowBrushRed = false;
        this.game.allowBrushGreen = false;
        this.game.allowBrushBlue = false;

        if (this.emptyFunctions)
            this.game.stack.initEmptyFunctions([6]);

        this.game.rocket.rotation = direction.RIGHT;
        this.game.createMap(...MAP_LVL10);
    }
    createMapLevel11() {
        this.configSize16();

        this.game.allowTurnLeft = true;
        this.game.allowMoveForward = true;
        this.game.allowTurnRight = true;
        this.game.allowFunction0 = true;
        this.game.allowFunction1 = false;
        this.game.allowFunction2 = false;
        this.game.allowConditionRed = true;
        this.game.allowConditionGreen = true;
        this.game.allowConditionBlue = true;
        this.game.allowBrushRed = false;
        this.game.allowBrushGreen = true;
        this.game.allowBrushBlue = false;

        if (this.emptyFunctions)
            this.game.stack.initEmptyFunctions([2, 5]);

        this.game.rocket.rotation = direction.LEFT;
        this.game.createMap(...MAP_LVL11);
    }

    createMapTest14526() {
        this.configSize16();

        // this.game.allowTurnLeft = true;
        // this.game.allowMoveForward = true;
        // this.game.allowTurnRight = true;
        // this.game.allowPaintRed = false;
        // this.game.allowPaintGreen = true;
        // this.game.allowPaintBlue = false;


        this.game.allowTurnLeft = true;
        this.game.allowMoveForward = true;
        this.game.allowTurnRight = true;
        this.game.allowFunction0 = false;
        this.game.allowFunction1 = false;
        this.game.allowFunction2 = false;
        this.game.allowConditionRed = false;
        this.game.allowConditionGreen = false;
        this.game.allowConditionBlue = false;
        this.game.allowBrushRed = false;
        this.game.allowBrushGreen = false;
        this.game.allowBrushBlue = false;


        if (this.emptyFunctions)
            this.game.stack.initEmptyFunctions([5]);

        this.game.rocket.rotation = direction.RIGHT;
        this.game.createMap(...MAP_TEST14526);
    }

    createMap4758() {
        this.configSize16();

        this.game.allowTurnLeft = true;
        this.game.allowMoveForward = true;
        this.game.allowTurnRight = true;
        this.game.allowFunction0 = true;
        this.game.allowFunction1 = true;
        this.game.allowFunction2 = true;
        this.game.allowConditionRed = true;
        this.game.allowConditionGreen = true;
        this.game.allowConditionBlue = true;
        this.game.allowBrushRed = false;
        this.game.allowBrushGreen = false;
        this.game.allowBrushBlue = false;

        if (this.emptyFunctions)
            this.game.stack.initEmptyFunctions([4, 3, 3]);

        this.game.rocket.rotation = direction.UP;
        this.game.createMap(
            210, // position de départ de la fusée
            [121, 209], // cellules avec étoile
            [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 32, 33, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 48, 49, 51, 52, 62, 64, 65, 67, 68, 70, 71, 72, 73, 74, 75, 76, 78, 80, 81, 83, 84, 86, 92, 94, 96, 97, 99, 100, 102, 104, 105, 106, 108, 110, 112, 113, 115, 116, 118, 120, 122, 124, 126, 128, 129, 131, 132, 134, 136, 138, 140, 142, 144, 145, 147, 148, 150, 152, 154, 156, 158, 160, 161, 163, 164, 166, 168, 172, 174, 176, 177, 179, 180, 182, 184, 185, 186, 187, 188, 190, 192, 193, 195, 196, 198, 206, 208, 212, 214, 215, 216, 217, 218, 219, 220, 221, 222, 224, 225, 228, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256], // cellules hors chemin
            [[18, 3], [19, 1], [20, 1], [21, 1], [22, 1], [23, 1], [24, 1], [25, 1], [26, 1], [27, 1], [28, 1], [29, 1], [30, 1], [31, 3], [34, 1], [47, 1]
                , [50, 1], [53, 3], [54, 1], [55, 1], [56, 1], [57, 1], [58, 1], [59, 1], [60, 1], [61, 3], [63, 1], [66, 1], [69, 1], [77, 1], [79, 1], [82, 1]
                , [85, 1], [87, 3], [88, 1], [89, 1], [90, 1], [91, 3], [93, 1], [95, 1], [98, 1], [101, 1], [103, 1], [107, 1], [109, 1], [111, 1], [114, 1], [117, 1]
                , [119, 1], [121, 2], [123, 1], [125, 1], [127, 1], [130, 1], [133, 1], [135, 1], [137, 1], [139, 1], [141, 1], [143, 1], [146, 1], [149, 1], [151, 1], [153, 1]
                , [155, 1], [157, 1], [159, 1], [162, 1], [165, 1], [167, 1], [169, 3], [170, 1], [171, 3], [173, 1], [175, 1], [178, 1], [181, 1], [183, 1], [189, 1], [191, 1]
                , [194, 1], [197, 1], [199, 3], [200, 1], [201, 1], [202, 1], [203, 1], [204, 1], [205, 3], [207, 1], [209, 2], [210, 1], [211, 3], [213, 1], [223, 1], [226, 3]
                , [227, 3], [229, 3], [230, 1], [231, 1], [232, 1], [233, 1], [234, 1], [235, 1], [236, 1], [237, 1], [238, 1], [239, 3]] // couleurs des cellules
        );
    }

    createMapUrl(urlParam: any) {

        // url http://localhost:5855/game?v=1&allow=111111111111&f=523&dir=UP&start=80&map=1,3;2,1;3,1&stars=1,2,3

        this.configSize16();

        // console.log('createMapUrl() urlParam', urlParam);
        const colors = this.getColorsPoints(urlParam);
        const OutOfPath = this.getOutOfPathPoints(urlParam);
        const stars = this.getStarsPoints(urlParam);


        this.allowConfig(urlParam);

        const fLength = urlParam.f.split('');
        if (this.emptyFunctions)
            this.game.stack.initEmptyFunctions(fLength);

            
        if (urlParam.dir === 'UP')
            this.game.rocket.rotation = direction.UP;
        else if (urlParam.dir === 'DOWN')
            this.game.rocket.rotation = direction.DOWN;
        else if (urlParam.dir === 'LEFT')
            this.game.rocket.rotation = direction.LEFT;
        else if (urlParam.dir === 'RIGHT')
            this.game.rocket.rotation = direction.RIGHT;


        const start = parseInt(urlParam.start,10);
        this.game.createMap(
            start, // position de départ de la fusée
            stars, // cellules avec étoile
            OutOfPath,
            colors
        );
    }


    getStarsPoints(urlParam: any)
    {
        let stars: any[] = urlParam.stars.split(',');
        stars = stars.map(str => +str);
        return stars;
    }
    getColorsPoints(urlParam: any)
    {
        let map = urlParam.map.split(';');
        
        const colors = [];
        for (const item of map) {
            const cellId = parseInt(item.split(',')[0],10);
            const colorId = parseInt(item.split(',')[1],10);
            colors.push([cellId, colorId]);
        }
        return colors;
    }
    
    getOutOfPathPoints(urlParam: any)
    {
        let map = urlParam.map.split(';');
        
        const outs = [];
        const cells = [];
        for (const item of map) {
            const cellId = parseInt(item.split(',')[0],10);
            cells.push(cellId);
        }
        for (let index = 1; index <= 16*16; index++) {
            
            if (!cells.includes(index)) outs.push(index);
        }
        return outs;
    }
    
    allowConfig(urlParam: any) {
        let allows = urlParam.allow.split('');
        let i = 0;
        for (const value of allows) {
            switch (i) {
                case 0:
                    this.game.allowTurnLeft = value === '1';
                    break;
                case 1:
                    this.game.allowMoveForward = value === '1';
                    break;
                case 2:
                    this.game.allowTurnRight = value === '1';
                    break;
                case 3:
                    this.game.allowFunction0 = value === '1';
                    break;
                case 4:
                    this.game.allowFunction1 = value === '1';
                    break;
                case 5:
                    this.game.allowFunction2 = value === '1';
                    break;
                case 6:
                    this.game.allowConditionRed = value === '1';
                    break;
                case 7:
                    this.game.allowConditionGreen = value === '1';
                    break;
                case 8:
                    this.game.allowConditionBlue = value === '1';
                    break;
                case 9:
                    this.game.allowBrushRed = value === '1';
                    break;
                case 10:
                    this.game.allowBrushGreen = value === '1';
                    break;
                case 11:
                    this.game.allowBrushBlue = value === '1';
                    break;
            }
            i++;
        }
    }

    configSize16() {
        this.game.size = 16;
        const grid: HTMLElement | null = document.querySelector('.grid-container');
        if (grid) {
            grid.style.gridTemplateColumns = 'repeat(auto-fill, minmax(6%, 1fr))';
            grid.style.gridTemplateRows = 'repeat(auto-fill, minmax(6%, 1fr))';
        }
    }
    configSize20() {
        this.game.size = 20;
        const grid: HTMLElement | null = document.querySelector('.grid-container');
        if (grid) {
            grid.style.gridTemplateColumns = 'repeat(auto-fill, minmax(5%, 1fr))';
            grid.style.gridTemplateRows = 'repeat(auto-fill, minmax(5%, 1fr))';
        }
    }
}