document.getElementById('upload').addEventListener('change', function(event) {
    const file = event.target.files[0];
    if (!file) {
        return;
    }

    const img = new Image();
    img.src = URL.createObjectURL(file);

    img.onload = function() {
        const canvas = document.getElementById('canvas');
        const ctx = canvas.getContext('2d');

        canvas.width = img.width;
        canvas.height = img.height;
        ctx.drawImage(img, 0, 0);

        const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
        const data = imageData.data;

        // Exemple : lire et afficher les valeurs RGBA du premier pixel
        const r = data[0];
        const g = data[1];
        const b = data[2];
        const a = data[3];

        console.log(`Premier pixel - Rouge: ${r}, Vert: ${g}, Bleu: ${b}, Alpha: ${a}`);

        // Parcourir tous les pixels
        for (let y = 0; y < canvas.height; y++) {
            for (let x = 0; x < canvas.width; x++) {
                const index = (y * canvas.width + x) * 4;
                const r = data[index];
                const g = data[index + 1];
                const b = data[index + 2];
                const a = data[index + 3];

                // Faites quelque chose avec les valeurs RGBA du pixel
                console.log(`Pixel (${x}, ${y}) - Rouge: ${r}, Vert: ${g}, Bleu: ${b}, Alpha: ${a}`);
            }
        }
    };
});
