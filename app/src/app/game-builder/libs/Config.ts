import { GameBuilderComponent } from "../game-builder.component";

export class Config {

    private _isHideMapObj: boolean = true;
    public builder: GameBuilderComponent;

    constructor(builder: GameBuilderComponent) {
        this.builder = builder;
    }

    onHideMapObj(): void {
        this._isHideMapObj = !this._isHideMapObj;
    }
    isHideMapObj(): boolean {
        return this._isHideMapObj;
    }
}