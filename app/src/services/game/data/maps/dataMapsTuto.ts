export const MAP_TUTO1: [number, number[], number[], [number, number][]] = [
    119, // position de départ de la fusée
    [122], // cellules avec étoile
    [
        ...row(1,117),
        ...row(123,256),
    ], // cellules hors chemin
    [
        [119,1],
        [120,1],
        [121,1],
        [122,1]
    ] // couleurs des cellules
];
export const MAP_TUTO2: [number, number[], number[], [number, number][]] = [
    119, // position de départ de la fusée
    [137], // cellules avec étoile
    [
        ...row(1,117),
        ...row(122,135),
        ...row(138,256),
    ], // cellules hors chemin
    [
        [119,2],
        [120,2],
        [121,2],
        [137,2]
    ] // couleurs des cellules
];

export const MAP_TUTO3: [number, number[], number[], [number, number][]] = [
    119, // position de départ de la fusée
    [106], // cellules avec étoile
    [
        ...row(1,104),
        ...row(107,117),
        ...row(123,256)
    ], // cellules hors chemin
    [
        [119,3],
        [120,3],
        [121,3],
        [122,3],
        [106,3]
    ] // couleurs des cellules
];

export const MAP_TUTO4: [number, number[], number[], [number, number][]] = [
    89, // position de départ de la fusée
    [152], // cellules avec étoile
    [
        ...row(1,87),
        ...row(90,95),
        ...row(96,103),
        ...row(106,111),
        ...row(112,119),
        ...row(122,127),
        ...row(129,135),
        ...row(138,143),
        ...row(145,150),
        ...row(154,256),
    ], // cellules hors chemin
    [[152,3],[153,2],[137,3],[121,3],[105,3],[89,3]] // couleurs des cellules
];

function row(rangeA: number, rangeB: number): number[]
{
    const arr = [];
    for (let index = rangeA; index-1 <= rangeB; index++) {
        arr.push(index);
    }
    return arr;
}