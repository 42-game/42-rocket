import { Component, OnInit } from '@angular/core';
import { Game } from 'src/services/Game';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent extends Game implements OnInit  {




  ngOnInit(): void {
    
    
    this.maps.create();
    
  }

  
  
}
