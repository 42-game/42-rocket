import { ElementRef } from "@angular/core";

export class Canvas {

    private w: number;
    private h: number;
    private canvas;
    private ctx!: CanvasRenderingContext2D | null;
    private colors!: string[];
    private mapColor!: any[];

    constructor(canvas: ElementRef<HTMLCanvasElement>, w: number = 16, h: number = 16) {
        this.canvas = canvas.nativeElement;
        this.canvas.addEventListener('click', this.handleCanvasClick.bind(this));

        this.canvas.width = w;
        this.canvas.height = h;
        this.w = w;
        this.h = h;

        this.ctx = this.canvas.getContext('2d');
        if (!this.ctx) {
            return;
        }
        this.ctx.fillStyle = 'rgba(0, 0, 0, 0)';
        this.ctx.fillRect(0, 0, this.w, this.h);
    }

    public onClick(callback: () => void) {
        return this.canvas.addEventListener('click', callback);
    }

    public setBackgroundColor(color: string): void {
        if (!this.ctx) {
            return;
        }
        this.ctx.fillStyle = color;
        this.ctx.fillRect(0, 0, this.w, this.h);
    }

    public configColors(colors: string[]): void {
        this.colors = colors;
    }

    public mapColorReader(callback: (i: number, color: string) => void): void {
        if (!this.ctx) {
            return;
        }
        const imageData = this.ctx.getImageData(0, 0, this.canvas.width, this.canvas.height);
        const data = imageData.data;


        let i = 1;

        for (let y = 0; y < this.canvas.height; y++) {
            for (let x = 0; x < this.canvas.width; x++) {
                const index = (y * this.canvas.width + x) * 4;
                const r = data[index];
                const g = data[index + 1];
                const b = data[index + 2];
                const a = data[index + 3];

                // console.log(`Cellule ${i} - Pixel (${x}, ${y}) - Rouge: ${r}, Vert: ${g}, Bleu: ${b}, Alpha: ${a}`);

                let color: string = this.getColor(r, g, b, a);
                callback(i, color);

                i++;
            }
        }
    }



    private handleCanvasClick(event: MouseEvent): void {
        const rect = this.canvas.getBoundingClientRect();
        const x = Math.floor((event.clientX - rect.left) / (rect.width / 16));
        const y = Math.floor((event.clientY - rect.top) / (rect.height / 16));
        // console.log(this.canvas.id, 'x', x, 'y', y);
        this.changeColor(x, y);
    }

    private changeColor(x: number, y: number): void {
        if (!this.ctx) {
            return;
        }
        const imageData = this.ctx.getImageData(x, y, 1, 1);
        const data = imageData.data;

        console.log('agrs color, ',data[0], data[1], data[2], data[3]);
        let color: string = this.getColor(data[0], data[1], data[2], data[3]);

        console.log('color', color);
        console.log('this.colors', this.colors);

        const currentIndex = this.colors.findIndex(c => c === color);
        let nextIndex = this.colors.length - 1 >= currentIndex + 1 ? currentIndex + 1 : 0;

        console.log('set colors', this.colors[nextIndex]);
        if (this.colors[nextIndex] === 'transparent') {
            this.ctx.clearRect(x, y, 1, 1);
        } else {
            this.ctx.fillStyle = this.colors[nextIndex];
            this.ctx.fillRect(x, y, 1, 1);
        }
        
        
        
    }

    private getColor(r: number, g: number, b: number, a: number): string {
        let redColor = r >= 255;
        let greenColor = g == 128 || g == 255;
        let blueColor = b >= 255;

        let color: string = 'black';
        
        if (a === 0) { color = 'transparent'}
        else if (redColor && !greenColor && !blueColor) {
            color = 'red';
        }
        else if (!redColor && greenColor && !blueColor) {
            color = 'green';
        }
        else if (!redColor && !greenColor && blueColor) {
            color = 'blue';
        }
        else if (redColor && greenColor && !blueColor) {
            color = 'yellow';
        }
        else if (redColor && !greenColor && blueColor) {
            color = 'magenta';
        }
        return color;
    }

    reDrawMap(points: any[]): void {
        if (!this.ctx) {
            return;
        }
        
        for (const point of points) {

            const idCell = point[0];
            const idColor = point[1];
            this.ctx.fillStyle = this.colors[idColor];
            const x = (idCell%16)-1;
            const y = Math.ceil(idCell / 16)-1 ;
            console.log('Canvas::reDrawMap() x', x, 'y', y);
            this.ctx.fillRect(x, y, 1, 1); 
        }
    }
    reDrawMap2(start: number, stars: number[]): void {
        if (!this.ctx) {
            return;
        }
        
        const startX = (start%16)-1;
        const startY = Math.ceil(start / 16)-1 ;
        this.ctx.fillStyle = this.colors[2];
        this.ctx.fillRect(startX, startY, 1, 1); 
        
        for (const celluleID of stars) {
            
            this.ctx.fillStyle = this.colors[1];
            const x = (celluleID%16)-1;
            const y = Math.ceil(celluleID / 16)-1 ;
            this.ctx.fillRect(x, y, 1, 1); 
        }
    }
}