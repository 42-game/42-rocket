import { Game } from "../Game";
export class Reader {

    isPlay: boolean = false;
    messageShow: boolean = false;
    messageText: string = '';
    idLoop: any;
    audioPlayer: any;
    constructor(private game: Game) {

        this.game = game;
    }

    messageHide() {
        this.messageShow = false;

        this.game.currentInst.f = 0;
        this.game.currentInst.item = 0;

        // this.game.stack.clear();
        this.game.maps.reset();
        this.setAudioSource('/assets/sounds/sound-background-2.mp3');
    }

    setAudioSource(src: string, loop: boolean = false) {
        if (this.game.audioPlayer && this.game.audioPlayer.nativeElement) {
            const audioElement = this.game.audioPlayer.nativeElement;
            audioElement.loop = loop;
            audioElement.src = src;
            audioElement.load();
            audioElement.play();
        } else {
            console.log('Echec audio play');
        }
    }

    loopStop() {

        
        if (this.messageText === 'Perdu')
            this.setAudioSource('/assets/sounds/videogame-death-sound-43894.mp3');

        if (this.messageText === 'Fin des instructions')
            this.setAudioSource('/assets/sounds/hotel-bell-ding-1-174457.mp3');



        this.isPlay = false;
        clearInterval(this.idLoop);

        this.game.currentInst.f = 0;
        this.game.currentInst.item = 0;
        this.game.maps.reset();
    }
    loopPlay() {
        this.isPlay = true;

        clearInterval(this.idLoop);
        this.idLoop = setInterval(this.execInstructions.bind(this), this.game.speed);
        this.game.stack.unSelectCells();
    }

    playStepByStep() {
        this.execInstructions.bind(this)();
        this.game.stack.unSelectCells();
    }

    playSpeedChange() {
        if (this.game.speed === 1000) this.game.speed = 500;
        else if (this.game.speed === 2000) this.game.speed = 1000;
        else if (this.game.speed === 500) this.game.speed = 200;
        else if (this.game.speed === 200) this.game.speed = 100;
        else if (this.game.speed === 100) this.game.speed = 2000;

        console.log('speed game', this.game.speed);
    }

    nextInstruction() {
        const f = this.game.currentInst.f;
        const item = this.game.currentInst.item;

        if ((item + 1) < this.game.inst[f].length) {
            this.game.currentInst.item = (item + 1) % this.game.inst[f].length;
            return true;
        } else
            return false
    }

    getCellColor() {
        let cellColor = 0;
        if (this.game.movePos.isOnRedColor()) {
            cellColor = 1;
        } else
            if (this.game.movePos.isOnGreenColor()) {
                cellColor = 2;
            } else
                if (this.game.movePos.isOnBlueColor()) {
                    cellColor = 3;
                }
        return cellColor;
    }

    playerWin() {
        let cell = this.game.movePos.getCell();
        cell.star = null;
        const findStar = this.game.grid.filter((c) => c.star === true);
        return findStar.length <= 0;
    }

    isOutOfPath() {
        let cell = this.game.movePos.getCell();
        return cell.disable;
    }


    execInstructions() {




        const { f, item } = this.game.currentInst;
        let cell = this.game.movePos.getCell();

        const {
            move,
            func,
            color,
            paint
        } = this.game.inst[f][item];
        
        if (move === 'Left') {
            if (cell.color === color || color === null) this.game.movePos.turnLeft();
        }
        if (move === 'Up') {
            if (cell.color === color || color === null) this.game.movePos.up();
        }
        if (move === 'Right') {
            if (cell.color === color || color === null) this.game.movePos.turnRight();
        }
        if (move === null && func !== null) {
            if (cell.color === color || color === null) {
                this.game.currentInst.f = func;
                this.game.currentInst.item = -1;
            }
        }

        if (paint && cell.color != paint) {
            if (cell.color === color || color === null) cell.color = paint;
        }

        if (this.isOutOfPath()) {



            this.messageShow = true;
            this.messageText = 'Perdu';
            this.loopStop();
        }



        if (this.playerWin()) {
            this.messageShow = true;
            this.messageText = 'Gagné !';
            this.loopStop();
            return;
        }


        if (!this.nextInstruction()) {

            this.messageShow = true;
            this.messageText = 'Fin des instructions';
            this.loopStop();
        }
    }
}