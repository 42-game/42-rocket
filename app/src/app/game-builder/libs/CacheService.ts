// https://developer.mozilla.org/en-US/docs/Web/API/IDBDatabase
// https://developer.mozilla.org/en-US/docs/Web/API/IDBObjectStore
export default class CacheService {
    private db: IDBDatabase | null;

    constructor() {
        this.db = null;
    }

    async getConnexion(): Promise<IDBDatabase> {
        await this.dbCreate();
        const db = await this.dbConnect();
        this.db = db;
        return db;
    }

    async getItem(name: string): Promise<any> {
        await this.getConnexion();

        const transaction = this.db!.transaction('storage', "readonly");
        const objectStore = transaction.objectStore('storage');

        const data = await this.dbGetItem(objectStore, name);
        this.db!.close();

        if (data === undefined) return undefined;

        return Object.prototype.hasOwnProperty.call(data, 'data') ? data.data : undefined;
    }

    async setItem(name: string, value: any): Promise<any> {
        await this.getConnexion();

        const transaction = this.db!.transaction('storage', "readwrite");
        const objectStore = transaction.objectStore('storage');

        const data = await this.dbSetItem(objectStore, { key: name, data: value });

        this.db!.close();

        return data;
    }

    async removeItem(name: string): Promise<any> {
        await this.getConnexion();

        const transaction = this.db!.transaction('storage', "readwrite");
        const objectStore = transaction.objectStore('storage');

        const data = await this.dbRemoveItem(objectStore, name);

        this.db!.close();

        return data;
    }

    async initTest(): Promise<void> {
        await this.dbCreate();
        const db = await this.dbConnect();
        const transaction = db.transaction('storage', "readwrite");

        const objectStore = transaction.objectStore('storage');

        await this.dbSetItem(objectStore, { key: 'key1', data: 'test1' });

        db.close();
    }

    dbCreate(): Promise<boolean> {
        const DBOpenRequest = window.indexedDB.open("mydatabase", 4);

        return new Promise((resolve, reject) => {
            DBOpenRequest.onupgradeneeded = function (event) {
                try {
                    const db = (event.target as IDBOpenDBRequest).result;
                    const objectStore = db.createObjectStore("storage", { keyPath: "key" });
                    objectStore.createIndex("data", "data", { unique: false });

                    console.log('Database created success.');
                    resolve(true);
                } catch (error) {
                    console.log('Database created error.');
                    reject(false);
                }
            };

            DBOpenRequest.onsuccess = () => resolve(true);
            DBOpenRequest.onerror = () => reject(false);
        });
    }

    dbConnect(): Promise<IDBDatabase> {
        const DBOpenRequest = window.indexedDB.open("mydatabase", 4);

        return new Promise((resolve, reject) => {
            DBOpenRequest.onsuccess = function (event) {
                const db = (event.target as IDBOpenDBRequest).result;
                resolve(db);
            };
            DBOpenRequest.onerror = () => {
                console.error('Database connexion failed.');
                reject(false);
            };
        });
    }

    dbGetItem(objectStore: IDBObjectStore, keyName: string): Promise<any> {
        const objectStoreTitleRequest = objectStore.get(keyName);
        return new Promise((resolve, reject) => {
            objectStoreTitleRequest.onsuccess = function (event) {
                const data = (event.target as IDBRequest).result;
                resolve(data);
            };
            objectStoreTitleRequest.onerror = () => {
                console.error('objectStore get failed.');
                reject(false);
            };
        });
    }

    dbSetItem(objectStore: IDBObjectStore, data: { key: string, data: any }): Promise<boolean> {
        const IDBRequest = objectStore.put(data);
        return new Promise((resolve, reject) => {
            IDBRequest.onsuccess = () => resolve(true);
            IDBRequest.onerror = () => {
                console.error('objectStore put error.');
                reject(false);
            };
        });
    }

    dbRemoveItem(objectStore: IDBObjectStore, name: string): Promise<boolean> {
        const IDBRequest = objectStore.delete(name);
        return new Promise((resolve, reject) => {
            IDBRequest.onsuccess = () => {
                console.log('objectStore delete success.');
                resolve(true);
            };
            IDBRequest.onerror = () => {
                console.error('objectStore delete error.');
                reject(false);
            };
        });
    }
}
