import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Config } from './libs/Config';
import { Canvas } from './libs/Canvas';
import { getCode } from './data/code';
import CacheService from './libs/CacheService';

@Component({
  selector: 'app-game-builder',
  templateUrl: './game-builder.component.html',
  styleUrls: ['./game-builder.component.scss']
})
export class GameBuilderComponent implements OnInit {

  @ViewChild('canvasMap', { static: true }) canvasMap!: ElementRef<HTMLCanvasElement>;
  @ViewChild('canvasObj', { static: true }) canvasObj!: ElementRef<HTMLCanvasElement>;

  public code: string;
  public outOfMap: number[];
  public colorsCondition: [i: number, colorIndex: number][];
  public start: number;
  public stars: number[];
  public allow: { [key: string]: boolean } = {};
  public functionsLength: number[];
  public rocketDirection: string = 'LEFT';
  public renderMap!: Canvas;
  public renderObj!: Canvas;
  public urlGame: string = '';
  private cacheService: CacheService;

  public config;
  constructor() {
    this.config = new Config(this);
    this.code = '';
    this.outOfMap = [];
    this.colorsCondition = [];
    this.start = 0;
    this.stars = [];
    this.allow = {};
    this.functionsLength = [0,0,0];


    this.cacheService = new CacheService();
    this.cacheService.getItem('code-builder')
    .then( data => {
      console.log('code-builder data', data);
      if (data) {
        this.start = data.start;
        this.colorsCondition = data.colorsCondition;
        this.stars = data.stars;
        this.allow = data.allow;
        this.functionsLength = data.functionsLength;
        this.outOfMap = data.outOfMap;
        this.rocketDirection = data.rocketDirection;


        console.log('constructor() this.colorsCondition', this.colorsCondition);
        this.reDrawMap(this.colorsCondition);
        this.reDrawMap2(this.start, this.stars);
        this.createUrlGame(data);
      }
    });



  }

  ngOnInit(): void {
    this.renderMap = new Canvas(this.canvasMap);
    this.renderObj = new Canvas(this.canvasObj);

    this.renderMap.setBackgroundColor('black');
    this.renderMap.configColors(['black', 'red', 'green', 'blue']);

    this.renderObj.configColors(['transparent', 'yellow', 'magenta']);


    this.renderMap.onClick(() => {

      this.outOfMap = [];
      this.renderMap.mapColorReader((i, color) => {
        
        if (color === 'black') {
          this.outOfMap.push(i);
        }
      });
      this.updateCode();
    })

    this.renderMap.onClick(() => {

      this.colorsCondition = [];

      this.renderMap.mapColorReader((i, color) => {
        if (color !== 'black') {
          
          if (color == 'red')
            this.colorsCondition.push([i, 1]);
          else if (color == 'green')
            this.colorsCondition.push([i, 2]);
          else if (color == 'blue')
            this.colorsCondition.push([i, 3]);
        }
      });
      this.updateCode();
    })

    this.renderObj.onClick(() => {
      
      this.stars = [];
      this.renderObj.mapColorReader((i, color) => {
        if (color === 'magenta') this.start = i;
        else if (color === 'yellow'){

          this.stars.push(i);
        }
      });
      this.updateCode();
    })

  }

  updateCode()
  {
      const data = {
        start: this.start ,
        colorsCondition: this.colorsCondition,
        stars: this.stars,
        allow: this.allow,
        functionsLength: this.functionsLength,
        outOfMap: this.outOfMap,
        rocketDirection: this.rocketDirection
      }
      this.code = getCode(data)
      this.createUrlGame(data);
  }

  onCheckbox(event: Event) {
    const checkbox = event.target as HTMLInputElement;
    const name = checkbox.name;
    const isChecked = checkbox.checked;

    this.allow[name] = isChecked;
    this.updateCode();
  }


  onChangefnLength(event: Event) {
    const input = event.target as HTMLInputElement;
    const value = input.value;
    const name = input.name;
    
    if(name === 'func0Length') {
      this.functionsLength[0] = +value;
    }
    else if(name === 'func1Length') {
      this.functionsLength[1] = +value;
    }
    else if(name === 'func2Length') {
      this.functionsLength[2] = +value;
    }
    this.updateCode();
  }

  onChangeDirection(dir: string) {
    this.rocketDirection = dir;
    this.updateCode();
  }

  reDrawMap(points: any[]): void {
    this.renderMap.reDrawMap(points);
  }
  reDrawMap2(start: number, stars: number[]): void {
    this.renderObj.reDrawMap2(start, stars);
  }

  createUrlGame(data: any): void {

    let allow = '';
    allow += this.allow['allowTurnLeft'] ? '1' : '0';
    allow += this.allow['allowMoveForward'] ? '1' : '0';
    allow += this.allow['allowTurnRight'] ? '1' : '0';

    allow += this.allow['allowFunction0'] ? '1' : '0';
    allow += this.allow['allowFunction1'] ? '1' : '0';
    allow += this.allow['allowFunction2'] ? '1' : '0';

    allow += this.allow['allowConditionRed'] ? '1' : '0';
    allow += this.allow['allowConditionGreen'] ? '1' : '0';
    allow += this.allow['allowConditionBlue'] ? '1' : '0';

    allow += this.allow['allowBrushRed'] ? '1' : '0';
    allow += this.allow['allowBrushGreen'] ? '1' : '0';
    allow += this.allow['allowBrushBlue'] ? '1' : '0';
    

    const f = ''+this.functionsLength[0]+this.functionsLength[1]+this.functionsLength[2];

    const dir = this.rocketDirection;
    const start = this.start;

    let map = [];
    let _map = '';
    for (const cellColor of this.colorsCondition) {
      map.push(`${cellColor[0]},${cellColor[1]}`)
    }
    _map = map.join(';');

    let stars = this.stars.join(',');

    const url = `/game?v=1&allow=${allow}&f=${f}&dir=${dir}&start=${start}&map=${_map}&stars=${stars}`;
    this.urlGame = url;
  }

  clearAll() {
    this.cacheService.removeItem('code-builder');
  }

}
