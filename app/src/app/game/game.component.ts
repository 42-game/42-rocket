import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Game } from 'src/services/Game';
import { Reader } from 'src/services/game/Reader';
@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent extends Game implements OnInit{

  @ViewChild('audioPlayer', { static: true }) audioPlayer2!: ElementRef<HTMLAudioElement>;
  

  constructor(private route: ActivatedRoute) {
    super(); 
  }
  
  ngOnInit(): void {

    this.reader = new Reader(this);

    this.setAudioPlayer(this.audioPlayer2);
    
    this.route.queryParams.subscribe(params => {
      if (params['v']) {
        this.maps.createMapUrl(params);
      } else {
        this.maps.create();
      }
    });
  }
}
