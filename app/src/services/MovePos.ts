import { Game } from "./Game";

export class MovePos
{
    pos!: number;
    size!: number;
    rocket!: any;
    grid!: any[];
    game!: Game;

    constructor(game: Game, grid: any[], size: number, rocket: any)
    {
        this.size = size;
        this.rocket = rocket;
        this.game = game;
        this.game.grid = this.game.grid;
    }

    up()
    {
        const lastPos = this.getPosItem();

        if      (this.game.rocket.rotation === 0) this.posUp();
        else if (this.game.rocket.rotation === 90) this.posRight();
        else if (this.game.rocket.rotation === -90) this.posLeft();
        else if (this.game.rocket.rotation === 180) this.posDown();
        
        const newPos = this.getPosItem();

        this.game.grid[lastPos].rocket = false;
        this.game.grid[newPos].rocket = true;
    }

    turnLeft()
    {
        if      (this.game.rocket.rotation === 0) this.game.rocket.rotation = -90;
        else if (this.game.rocket.rotation === 90) this.game.rocket.rotation = 0;
        else if (this.game.rocket.rotation === -90) this.game.rocket.rotation = 180;
        else if (this.game.rocket.rotation === 180) this.game.rocket.rotation = 90;
        
    }

    turnRight()
    {
        if      (this.game.rocket.rotation === 0) this.game.rocket.rotation = 90;
        else if (this.game.rocket.rotation === 90) this.game.rocket.rotation = 180;
        else if (this.game.rocket.rotation === -90) this.game.rocket.rotation = 0;
        else if (this.game.rocket.rotation === 180) this.game.rocket.rotation = -90;
    }
    
    posUp()
    {
        this.game.rocket.pos -= this.game.size;
        return this;
    }

    posDown()
    {
        this.game.rocket.pos += this.game.size;
        return this;
    }

    posLeft()
    {
        this.game.rocket.pos -= 1;
        return this;
    }

    posRight()
    {
        this.game.rocket.pos += 1;
        return this;
    }

    getPosItem()
    {
        return this.game.rocket.pos-1;
    }

    getRotation()
    {
        return this.game.rocket.rotation;
    }
    

    isOnRedColor()
    {
        const pos = this.getPosItem();
        return this.game.grid[pos].color === 1;
    }
    isOnGreenColor()
    {
        const pos = this.getPosItem();
        return this.game.grid[pos].color === 2;
    }
    isOnBlueColor()
    {
        const pos = this.getPosItem();
        return this.game.grid[pos].color === 3;
    }

    getCell(){
        const pos = this.getPosItem();
        return this.game.grid[pos];
    }
}