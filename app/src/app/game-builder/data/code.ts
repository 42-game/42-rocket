import CacheService from "../libs/CacheService";


export function getCode(args: {
    start: number, 
    colorsCondition: [i: number, colorIndex: number][],
    stars: number[],
    allow: { [key: string]: boolean },
    functionsLength: number[],
    outOfMap: number[],
    rocketDirection: string
}): string {

    const {
        start,
        colorsCondition,
        stars,
        allow,
        functionsLength,
        outOfMap,
        rocketDirection
    } = args;

    setSave(args);
    

    // chaine de caractère qui affiche les cellules de couleurs sur la carte
    let colorsCond = '';
    let arr = [];
    let i = 1;
    for(const color of colorsCondition) {
        const c = `[${color[0]},${color[1]}]`;
        arr.push(i %16 === 0 ? c+"\n" : c );
        i++;
    }
    colorsCond = arr.join(',');
    
    // chaine de caractère qui affiche les étoiles sur la carte
    let _stars = stars.join(',');
    // chaine de caractère qui indique le nombre d'instruction par fonction.
    let _functionsLength = functionsLength.join(',');
    let _outOfMap = outOfMap.join(',');

    const code = `
    createMapX()
    {
            this.configSize16();
    
            this.game.allowTurnLeft = ${ allow['allowTurnLeft'] ? 'true' : 'false'};
            this.game.allowMoveForward = ${ allow['allowMoveForward'] ? 'true' : 'false'};
            this.game.allowTurnRight = ${ allow['allowTurnRight'] ? 'true' : 'false'};
            this.game.allowFunction0 = ${ allow['allowFunction0'] ? 'true' : 'false'};
            this.game.allowFunction1 = ${ allow['allowFunction1'] ? 'true' : 'false'};
            this.game.allowFunction2 = ${ allow['allowFunction2'] ? 'true' : 'false'};
            this.game.allowConditionRed = ${ allow['allowConditionRed'] ? 'true' : 'false'};
            this.game.allowConditionGreen = ${ allow['allowConditionGreen'] ? 'true' : 'false'};
            this.game.allowConditionBlue = ${ allow['allowConditionBlue'] ? 'true' : 'false'};
            this.game.allowBrushRed = ${ allow['allowBrushRed'] ? 'true' : 'false'};
            this.game.allowBrushGreen = ${ allow['allowBrushGreen'] ? 'true' : 'false'};
            this.game.allowBrushBlue = ${ allow['allowBrushBlue'] ? 'true' : 'false'};
            
            if(this.emptyFunctions)
                this.game.stack.initEmptyFunctions([${_functionsLength}]);
    
            this.game.rocket.rotation = direction.${rocketDirection};
            this.game.createMap(
                ${start}, // position de départ de la fusée
                [${_stars}], // cellules avec étoile
                [${_outOfMap}], // cellules hors chemin
                [${colorsCond}] // couleurs des cellules
            );
    }
    `;
    return code;
}

async function setSave(args: any) {
    const cacheService = new CacheService();
    const data = await cacheService.setItem('code-builder', args);
}
async function getSave() {
    const cacheService = new CacheService();
    return cacheService.getItem('code-builder');
}