import { Game } from "../Game";

export class Stack {


    constructor(private game: Game) {
        this.game = game;
    }

    clear() {
        const elm: any = { ...this.game.instDefault };

        for (const func of this.game.inst) {
            for (let item of func) {
                for (const key of Object.keys(elm)) {
                    item[key] = elm[key];
                }
            }
        }
    }

    unSelectCells() {
        for (const func of this.game.inst) {
            for (const item of func) {
                item.selected = false;
            }
        }
    }

    select(indexFunc: number, nInst: number) {
        this.game.instSelected.f = indexFunc;
        this.game.instSelected.item = nInst;

        const value = this.game.inst[indexFunc][nInst].selected;

        this.unSelectCells();

        this.game.inst[indexFunc][nInst].selected = !value;
    }

    initEmptyFunctions(buildFunc: any[]) {

        this.game.inst[0] = [];
        this.game.inst[1] = [];
        this.game.inst[2] = [];
        
        for (let index = 0; index < buildFunc.length; index++) {
            const value = buildFunc[index];

            this.game.inst[index] = [];
            for (let i = 0; i < value; i++) this.game.inst[index].push({ ...this.game.instDefault });
        }


        // this.game.inst[0] = [];
        // for (let i = 0; i < this.game.f0Length; i++) this.game.inst[0].push({ ...this.game.instDefault });
        // this.game.inst[1] = [];
        // for (let i = 0; i < this.game.f1Length; i++) this.game.inst[1].push({ ...this.game.instDefault });
        // this.game.inst[2] = [];
        // for (let i = 0; i < this.game.f2Length; i++) this.game.inst[2].push({ ...this.game.instDefault });
    }
}