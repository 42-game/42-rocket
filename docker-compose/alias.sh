#!/bin/bash

alias up="docker compose up -d"
alias down="docker compose down"
alias cmd="docker compose exec app bash"
alias build="docker compose build"
