import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PixelsToMapComponent } from './pixels-to-map/pixels-to-map.component';
import { GameComponent } from './game/game.component';
import { GameBuilderComponent } from './game-builder/game-builder.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    PixelsToMapComponent,
    GameComponent,
    GameBuilderComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
